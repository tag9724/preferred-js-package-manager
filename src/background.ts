// import { getChromeStorageLocalAsync } from 'utils'
import { getStoreUserPreferences } from './modules'

const userPreferences = await getStoreUserPreferences()

/* Open options on extension click */

chrome.browserAction.onClicked.addListener(() => chrome.runtime.openOptionsPage())

/* Context menus */

const contextMenus = {
  'npmjs-search'   : {
    title    : 'Search on npmjs',
    contexts : ['selection'],
    visible  : false,
    enabled  : userPreferences['toggle-context-menu-search-npmjs']
  },
  'yarnpkg-search' : {
    title    : 'Search on yarnpkg',
    contexts : ['selection'],
    visible  : false,
    enabled  : userPreferences['toggle-context-menu-search-yarnpkg']
  }
}

const updateEnabledContextMenus = (): void => {
  const entries = Object.entries(contextMenus)

  for (const [id, menu] of entries) {
    const displayed = menu.visible
    menu.visible = menu.enabled

    if (displayed && !menu.enabled) {
      chrome.contextMenus.remove(id)
      continue
    }

    if (!displayed && menu.enabled) {
      chrome.contextMenus.create({ ...menu, id: id })
    }
  }
}

const toggleContextMenu = (id: keyof typeof contextMenus, toggle: boolean): void => {
  contextMenus[id].enabled = toggle
  updateEnabledContextMenus()
}

chrome.contextMenus.onClicked.addListener((info: any) => {
  if (info.menuItemId === 'npmjs-search') {
    const query: string = encodeURIComponent(info.selectionText.trim().toLowerCase())
    const url = `https://www.npmjs.com/search?q=${query}`

    return chrome.tabs.create({ url })
  }

  if (info.menuItemId === 'yarnpkg-search') {
    const query: string = encodeURIComponent(info.selectionText.trim().toLowerCase())
    const url = `https://yarnpkg.com/?q=${query}`

    return chrome.tabs.create({ url })
  }
})

updateEnabledContextMenus()

/* Background Messaging */

chrome.runtime.onMessage.addListener((request: runtimeRequest, sender: any, sendResponse: any) => {
  if (request.type === 'contextMenuToggleNpmjs') {
    toggleContextMenu('npmjs-search', request.toggle as boolean)
    return true
  }

  if (request.type === 'contextMenuToggleYarnpkg') {
    toggleContextMenu('yarnpkg-search', request.toggle as boolean)
    return true
  }

  return false
})
