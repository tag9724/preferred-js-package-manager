import '@css/markdownModifiedCodeBlocks.css'
import { copyCodeBlockToClipboard } from './copyCodeBlockToClipboard'

export const appendMarkdownCodeBlockCopyButton = (codeBlockElement: HTMLElement): void => {
  if (codeBlockElement.dataset.pjpmCopyButton === 'true') return

  const button = document.createElement('button')
  button.classList.add('pjpm-button', 'pjpm-copy-button')
  button.setAttribute('title', 'Copy code block content')

  const icon = document.createElement('i')
  icon.classList.add('pjpm', 'far', 'fa-clipboard')
  button.appendChild(icon)

  let copyTimeout: number

  button.onclick = async () => {
    await copyCodeBlockToClipboard(codeBlockElement)

    button.classList.remove('copying')
    button.getBoundingClientRect()
    button.classList.add('copying')

    clearTimeout(copyTimeout)
    copyTimeout = window.setTimeout(() => button.classList.remove('copying'), 600)
  }

  codeBlockElement.dataset.pjpmCopyButton = 'true'
  codeBlockElement.insertAdjacentElement('beforebegin', button)
}

export const appendMarkdownCodeBlockReverseButton = (
  codeBlockElement: HTMLElement,
  originalContent: string,
  modifiedContent: string
): void => {
  if (codeBlockElement.dataset.pjpmReverseButton === 'true') return
  const modifiedTitle = 'Modified by web extension - Prefered js package manager'

  const button = document.createElement('button')
  button.classList.add('pjpm-button', 'pjpm-reverse-button')
  button.setAttribute('title', 'Switch between original and modified code block content')

  const icon = document.createElement('i')
  icon.classList.add('pjpm', 'fas')
  button.appendChild(icon)

  button.onclick = () => {
    const state = !codeBlockElement.classList.contains('pjpm-modified')
    changeState(state)
  }

  const changeState = (toggleModified: boolean): void => {
    codeBlockElement.setAttribute('title', toggleModified ? modifiedTitle : '')
    codeBlockElement.innerHTML = toggleModified ? modifiedContent : originalContent

    icon.classList.toggle('fa-undo', toggleModified)
    icon.classList.toggle('fa-redo', !toggleModified)

    codeBlockElement.classList.toggle('pjpm-modified', toggleModified)
  }

  codeBlockElement.classList.add('pjpm-install-code-block')
  codeBlockElement.dataset.pjpmReverseButton = 'true'

  changeState(true)
  codeBlockElement.insertAdjacentElement('beforebegin', button)
}
