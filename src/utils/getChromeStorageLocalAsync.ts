export const getChromeStorageLocalAsync = (entry: string): Promise<any> => {
  return new Promise(resolve => {
    chrome.storage.local.get(entry, (data: any) => resolve(data[entry]))
  })
}
