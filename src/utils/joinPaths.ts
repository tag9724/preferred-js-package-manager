/**
 * @example
 *
 * joinPaths( "/a/", "b//" )           // "/a/b"
 * joinPaths("a/", "/b/", "/c/", "d")  // "a/b/c/d"
 */

export const joinPaths = (...paths: string[]): string => {
  const start = paths.shift() as string
  const body = paths.map(p => p.replace(/^\/|\/+$/g, '')).join('/')

  return `${start}${body}`
}
