const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = (env, argv) => {
  const isProd = argv?.mode === 'production'
  console.info('Use Production Mode : ', isProd)

  return {
    context      : path.resolve(__dirname, '.'),

    entry        : {
      options                   : '/src/options.ts',
      background                : '/src/background.ts',
      npmJsPackage              : '/src/npmJsPackage.ts',
      yarnpkgJsPackage          : '/src/yarnpkgJsPackage.ts',
      injectFontAwesomeFontFace : '/src/injectFontAwesomeFontFace.ts'
    },

    output       : {
      path  : path.resolve(__dirname, 'dist'),
      clean : isProd
    },

    experiments  : {
      topLevelAwait : true
    },

    devtool      : isProd ? 'inline-source-map' : false,

    optimization : {
      minimize : false
    },

    plugins      : [new MiniCssExtractPlugin()],

    module       : {
      rules : [
        {
          test    : /\.m?js|.json/,
          resolve : {
            fullySpecified : false
          }
        },
        {
          test    : /\.tsx?$/,
          use     : ['ts-loader'],
          exclude : /node_modules/
        },

        {
          test : /\.css$/,
          use  : [MiniCssExtractPlugin.loader, { loader: 'css-loader', options: { url: false } }]
        },

        {
          test : /\.(png|svg|jpe?g|gif)$/,
          use  : ['file-loader']
        },

        {
          test : /\.(woff|woff2|eot|ttf|otf)$/,
          use  : ['file-loader']
        }
      ]
    },

    resolve      : {
      extensions : ['.ts', '.json'],

      alias      : {
        utils  : path.resolve(__dirname, 'src/utils'),
        '@css' : path.resolve(__dirname, 'src/css')
      }
    }
  }
}
