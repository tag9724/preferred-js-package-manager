import { appendMarkdownCodeBlockCopyButton } from '.'

export const traverseMarkdownCodeBlocks = (
  codeElements: HTMLElement[] | NodeListOf<HTMLElement>
): void => {
  for (const source of codeElements) {
    appendMarkdownCodeBlockCopyButton(source)
  }
}
