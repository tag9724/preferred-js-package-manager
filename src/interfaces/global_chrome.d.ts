interface chrome {
  loadTimes: any
  csi: any
  app: {
    isInstalled: boolean
    getDetails: any
    getIsInstalled: any
    installState: any
    runningState: any
    InstallState: {
      DISABLED: string
      INSTALLED: string
      NOT_INSTALLED: string
    }
    RunningState: {
      CANNOT_RUN: string
      READY_TO_RUN: string
      RUNNING: string
    }
  }
  browserAction: {
    disable: any
    enable: any
    getBadgeBackgroundColor: any
    getBadgeText: any
    getPopup: any
    getTitle: any
    openPopup: any
    setBadgeBackgroundColor: any
    setBadgeText: any
    setIcon: any
    setPopup: any
    setTitle: any
    onClicked: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
  }
  contextMenus: {
    create: any
    remove: any
    removeAll: any
    update: any
    ACTION_MENU_TOP_LEVEL_LIMIT: number
    onClicked: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    ContextType: {
      ACTION: string
      ALL: string
      AUDIO: string
      BROWSER_ACTION: string
      EDITABLE: string
      FRAME: string
      IMAGE: string
      LAUNCHER: string
      LINK: string
      PAGE: string
      PAGE_ACTION: string
      SELECTION: string
      VIDEO: string
    }
    ItemType: {
      CHECKBOX: string
      NORMAL: string
      RADIO: string
      SEPARATOR: string
    }
  }
  dom: {
    openOrClosedShadowRoot: any
  }
  extension: {
    getBackgroundPage: any
    getExtensionTabs: any
    getURL: any
    getViews: any
    isAllowedFileSchemeAccess: any
    isAllowedIncognitoAccess: any
    sendRequest: any
    setUpdateUrlData: any
    inIncognitoContext: boolean
    connect: any
    connectNative: any
    sendMessage: any
    sendNativeMessage: any
    onRequestExternal: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    onRequest: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    ViewType: {
      POPUP: string
      TAB: string
    }
    onConnect: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    onConnectExternal: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    onMessage: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    onMessageExternal: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
  }
  i18n: {
    detectLanguage: any
    getAcceptLanguages: any
    getMessage: any
    getUILanguage: any
  }
  management: {
    getPermissionWarningsByManifest: any
    getSelf: any
    uninstallSelf: any
    ExtensionDisabledReason: {
      PERMISSIONS_INCREASE: string
      UNKNOWN: string
    }
    ExtensionInstallType: {
      ADMIN: string
      DEVELOPMENT: string
      NORMAL: string
      OTHER: string
      SIDELOAD: string
    }
    ExtensionType: {
      EXTENSION: string
      HOSTED_APP: string
      LEGACY_PACKAGED_APP: string
      LOGIN_SCREEN_EXTENSION: string
      PACKAGED_APP: string
      THEME: string
    }
    LaunchType: {
      OPEN_AS_PINNED_TAB: string
      OPEN_AS_REGULAR_TAB: string
      OPEN_AS_WINDOW: string
      OPEN_FULL_SCREEN: string
    }
  }
  permissions: {
    contains: any
    getAll: any
    remove: any
    request: any
    onRemoved: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    onAdded: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
  }
  runtime: {
    id: string
    onMessageExternal: any
    onMessage: any
    onConnectExternal: any
    onConnect: any
    connect: any
    getBackgroundPage: any
    getManifest: any
    getPackageDirectoryEntry: any
    getPlatformInfo: any
    getURL: any
    openOptionsPage: any
    reload: any
    requestUpdateCheck: any
    restart: any
    restartAfterDelay: any
    sendMessage: any
    setUninstallURL: any
    onRestartRequired: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    onBrowserUpdateAvailable: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    onUpdateAvailable: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    onSuspendCanceled: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    onSuspend: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    onInstalled: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    onStartup: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    OnInstalledReason: {
      CHROME_UPDATE: string
      INSTALL: string
      SHARED_MODULE_UPDATE: string
      UPDATE: string
    }
    OnRestartRequiredReason: {
      APP_UPDATE: string
      OS_UPDATE: string
      PERIODIC: string
    }
    PlatformArch: {
      ARM: string
      ARM64: string
      MIPS: string
      MIPS64: string
      X86_32: string
      X86_64: string
    }
    PlatformNaclArch: {
      ARM: string
      MIPS: string
      MIPS64: string
      X86_32: string
      X86_64: string
    }
    PlatformOs: {
      ANDROID: string
      CROS: string
      LINUX: string
      MAC: string
      OPENBSD: string
      WIN: string
    }
    RequestUpdateCheckStatus: {
      NO_UPDATE: string
      THROTTLED: string
      UPDATE_AVAILABLE: string
    }
  }
  storage: {
    sync: {
      get: any
      set: any
      remove: any
      clear: any
      getBytesInUse: any
      QUOTA_BYTES: number
      QUOTA_BYTES_PER_ITEM: number
      MAX_ITEMS: number
      MAX_WRITE_OPERATIONS_PER_HOUR: number
      MAX_WRITE_OPERATIONS_PER_MINUTE: number
      MAX_SUSTAINED_WRITE_OPERATIONS_PER_MINUTE: number
      onChanged: {
        addListener: any
        removeListener: any
        hasListener: any
        hasListeners: any
        dispatch: any
      }
    }
    managed: {
      get: any
      set: any
      remove: any
      clear: any
      getBytesInUse: any
      onChanged: {
        addListener: any
        removeListener: any
        hasListener: any
        hasListeners: any
        dispatch: any
      }
    }
    local: {
      get: any
      set: any
      remove: any
      clear: any
      getBytesInUse: any
      QUOTA_BYTES: number
      onChanged: {
        addListener: any
        removeListener: any
        hasListener: any
        hasListeners: any
        dispatch: any
      }
    }
    onChanged: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
  }
  tabs: {
    captureVisibleTab: any
    connect: any
    create: any
    detectLanguage: any
    discard: any
    duplicate: any
    executeScript: any
    get: any
    getAllInWindow: any
    getCurrent: any
    getSelected: any
    getZoom: any
    getZoomSettings: any
    goBack: any
    goForward: any
    group: any
    highlight: any
    insertCSS: any
    move: any
    query: any
    reload: any
    remove: any
    removeCSS: any
    sendMessage: any
    sendRequest: any
    setZoom: any
    setZoomSettings: any
    ungroup: any
    update: any
    TAB_ID_NONE: number
    onZoomChange: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    onReplaced: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    onRemoved: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    onAttached: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    onDetached: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    onHighlighted: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    onHighlightChanged: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    onActivated: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    onActiveChanged: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    onSelectionChanged: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    onMoved: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    onUpdated: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    onCreated: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    MutedInfoReason: {
      CAPTURE: string
      EXTENSION: string
      USER: string
    }
    TabStatus: {
      COMPLETE: string
      LOADING: string
      UNLOADED: string
    }
    WindowType: {
      APP: string
      DEVTOOLS: string
      NORMAL: string
      PANEL: string
      POPUP: string
    }
    ZoomSettingsMode: {
      AUTOMATIC: string
      DISABLED: string
      MANUAL: string
    }
    ZoomSettingsScope: {
      PER_ORIGIN: string
      PER_TAB: string
    }
  }
  windows: {
    create: any
    get: any
    getAll: any
    getCurrent: any
    getLastFocused: any
    remove: any
    update: any
    WINDOW_ID_CURRENT: number
    WINDOW_ID_NONE: number
    onBoundsChanged: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    onFocusChanged: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    onRemoved: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    onCreated: {
      addListener: any
      removeListener: any
      hasListener: any
      hasListeners: any
      dispatch: any
    }
    CreateType: {
      NORMAL: string
      PANEL: string
      POPUP: string
    }
    WindowState: {
      FULLSCREEN: string
      LOCKED_FULLSCREEN: string
      MAXIMIZED: string
      MINIMIZED: string
      NORMAL: string
    }
    WindowType: {
      APP: string
      DEVTOOLS: string
      NORMAL: string
      PANEL: string
      POPUP: string
    }
  }
}

// eslint-disable-next-line @typescript-eslint/no-redeclare
declare const chrome: chrome
