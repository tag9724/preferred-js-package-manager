export const copyCodeBlockToClipboard = async (codeBlockElement: Element): Promise<void> => {
  const useComplexCopy = codeBlockElement.classList.contains('editor')

  if (useComplexCopy) {
    const nodes = Array.from(codeBlockElement.childNodes)
    const content = nodes.map(e => e.textContent).join('\n')
    await navigator.clipboard.writeText(content)

    return
  }

  const content = codeBlockElement.textContent as string
  await navigator.clipboard.writeText(content)
}
