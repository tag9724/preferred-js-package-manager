const gulp = require('gulp')

const del = require('del')
const zip = require('gulp-zip')

const prettier = require('gulp-prettier')
const zipName = 'preferred-js-package-manager.zip'

const taskBundleZip = async () => {
  await del(['./build/*.zip'], { dryRun: false })

  return gulp
    .src(['./dist/**/*', './assets/**/*', 'options.html', 'manifest.json'], { base: '.' })
    .pipe(zip(zipName))
    .pipe(gulp.dest('build'))
}

const taskPrettier = async () => {
  const options = {
    semi          : false,
    singleQuote   : true,
    printWidth    : 100,
    trailingComma : 'none'
  }

  return gulp
    .src(['./dist/**/*.js'], { base: '.' })
    .pipe(prettier(options))
    .pipe(gulp.dest('.'))
}

gulp.task('default', gulp.series(taskPrettier, taskBundleZip))
