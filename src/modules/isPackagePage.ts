export const isPackagePage = (): boolean => {
  const path = location.pathname
  return path.indexOf('/package/') === 0 || path.indexOf('/en/package/') === 0
}
