const commands: packageManagerList = {
  npm   : 'npm i',
  pnpm  : 'pnpm i',
  yarn  : 'yarn add',
  pyarn : 'pyarn add'
}

const saveArguments: packageManagerList = {
  npm   : '--save-dev',
  pnpm  : '--save-dev',
  yarn  : '--dev',
  pyarn : '--dev'
}

export const getPackageManagerInstallCommand = (packageManager: packageManager): string => {
  return commands[packageManager]
}

export const getPackageManagerInstallSaveArgument = (packageManager: packageManager): string => {
  return saveArguments[packageManager]
}
