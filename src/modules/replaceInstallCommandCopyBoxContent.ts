import { getPackageManagerInstallCommand } from '.'
import { getPackageManagerInstallSaveArgument } from './_getPackageManager'

export const replaceInstallCommandCopyBoxContent = (
  sourceElement: Element,
  targetElement: Element,
  packageManager: packageManager,
  includeSaveDevArgument = false
): void => {
  const originalContent = sourceElement.textContent as string
  const installCmd = getPackageManagerInstallCommand(packageManager)
  let command = installCmd

  if (includeSaveDevArgument) {
    const saveArgument = getPackageManagerInstallSaveArgument(packageManager)
    command = `${installCmd} ${saveArgument}`
  }

  targetElement.textContent = originalContent.replace(/^(npm i|yarn add)/, command)
}
