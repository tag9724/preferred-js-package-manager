interface userPreferences {
  'preferred-package-manager': packageManager
  'second-install-box-with-dev-argument': boolean
  'should-enforce-line-breaks': boolean
  'yarnpkg-auto-expand-readme': boolean
  'toggle-context-menu-search-npmjs': boolean
  'toggle-context-menu-search-yarnpkg': boolean
}
