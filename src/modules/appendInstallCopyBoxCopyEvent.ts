export const appendInstallCopyBoxCopyEvent = (copiedCodeInstallBox: HTMLElement): void => {
  let copyTimeout: number

  copiedCodeInstallBox.onclick = async () => {
    const text = copiedCodeInstallBox.textContent as string
    await navigator.clipboard.writeText(text.trim())

    copiedCodeInstallBox.classList.remove('copying')
    copiedCodeInstallBox.getBoundingClientRect()
    copiedCodeInstallBox.classList.add('copying')

    clearTimeout(copyTimeout)
    copyTimeout = window.setTimeout(() => {
      copiedCodeInstallBox.classList.remove('copying')
    }, 1500)
  }
}
