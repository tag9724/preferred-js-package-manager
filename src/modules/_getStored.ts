import { getChromeStorageLocalAsync } from 'utils'
import { setStoredPreferredPackageManager } from './_setStored'

const defaultPreferences: userPreferences = {
  'preferred-package-manager'            : 'pnpm',
  'second-install-box-with-dev-argument' : true,
  'should-enforce-line-breaks'           : false,
  'yarnpkg-auto-expand-readme'           : true,
  'toggle-context-menu-search-npmjs'     : true,
  'toggle-context-menu-search-yarnpkg'   : false
}

const defaultPreferencesKeys = Object.keys(defaultPreferences)

export const getStoreUserPreferences = (): Promise<userPreferences> => {
  return new Promise(resolve => {
    chrome.storage.local.get(defaultPreferencesKeys, (data: userPreferences) => {
      // TODO Will be removed in futur update ( Removing pyarn )
      if (data['preferred-package-manager'] === 'pyarn') {
        data['preferred-package-manager'] = 'yarn'
        setStoredPreferredPackageManager('yarn')
      }

      resolve({ ...defaultPreferences, ...data })
    })
  })
}

export const getStoredPreferredPackageManager = async (): Promise<packageManager> => {
  const stored = await getChromeStorageLocalAsync('preferred-package-manager')

  // TODO Will be removed in futur update ( Removing pyarn )
  if (stored === 'pyarn') {
    setStoredPreferredPackageManager('yarn')
    return 'yarn'
  }

  return stored ?? defaultPreferences['preferred-package-manager']
}

export const getStoredSecondInstallBoxWithDevArgument = async (): Promise<boolean> => {
  const stored: boolean | null = await getChromeStorageLocalAsync(
    'second-install-box-with-dev-argument'
  )
  return stored ?? defaultPreferences['second-install-box-with-dev-argument']
}

export const getStoredShouldEnforceLineBreaks = async (): Promise<boolean> => {
  const stored: boolean | null = await getChromeStorageLocalAsync('should-enforce-line-breaks')
  return stored ?? defaultPreferences['should-enforce-line-breaks']
}

export const getStoredYarnpkgAutoExpandReadme = async (): Promise<boolean> => {
  const stored: boolean | null = await getChromeStorageLocalAsync('yarnpkg-auto-expand-readme')
  return stored ?? defaultPreferences['yarnpkg-auto-expand-readme']
}

export const getStoredToggleContextMenuSearchNpmjs = async (): Promise<boolean> => {
  const stored: boolean | null = await getChromeStorageLocalAsync(
    'toggle-context-menu-search-npmjs'
  )
  return stored ?? defaultPreferences['toggle-context-menu-search-npmjs']
}

export const getStoredToggleContextMenuSearchYarnpkg = async (): Promise<boolean> => {
  const stored: boolean | null = await getChromeStorageLocalAsync(
    'toggle-context-menu-search-yarnpkg'
  )
  return stored ?? defaultPreferences['toggle-context-menu-search-yarnpkg']
}
