import '@css/markdownModifiedCodeBlocks.css'
import { appendMarkdownCodeBlockReverseButton } from '.'

import {
  getPackageManagerInstallCommand,
  getPackageManagerInstallSaveArgument
} from './_getPackageManager'

const managersRegexp = [
  // (p)npm (i)nstall --save-dev
  // (p)npm (i)nstall --save
  // (p)npm (i)nstall --<span>save<span>-<span>dev<span>
  // -- GROUP 2 --
  // (p)npm (i)nstal
  /(p?npm(?:\s+|&nbsp;)(?:install|i)).*(--save-dev|--save|--<span.*?>save<\/span>-<span.*?>dev<\/span>)|(p?npm(?:\s+|&nbsp;)(?:install|i))/g,
  // (p)yarn add --dev
  // (p)yarn add --prefer-dev
  // -- GROUP 2 --
  // (p)yarn add
  /(p?yarn(?:\s+|&nbsp;)add).*(--dev|--prefer-dev)|(p?yarn(?:\s+|&nbsp;)add)/g
]

export const traverseMarkdownInstallCommands = (
  codeElements: HTMLElement[] | NodeListOf<HTMLElement>,
  packageManager: packageManager
): void => {
  const installCommand = getPackageManagerInstallCommand(packageManager)
  const saveArgument = getPackageManagerInstallSaveArgument(packageManager)

  for (const source of codeElements) {
    const original = source.innerHTML
    let content = original

    for (const manager of managersRegexp) {
      content = content.replace(manager, (match, install1, save, install2) => {
        // Don't have save argument
        if (install2 !== undefined) return match.replace(install2, installCommand)

        // Have save argument
        match = match.replace(install1, installCommand)

        // Remove "--save" from npm command ( it's not need and conflict with yarn )
        if (save === '--save') return match.replace(save, '')

        // Or replace the --save-dev with equivalent
        return match.replace(save, saveArgument)
      })
    }

    if (original !== content) {
      appendMarkdownCodeBlockReverseButton(source, original, content)
    }
  }
}
