export const setStoredPreferredPackageManager = (packageManager: packageManager): void => {
  chrome.storage.local.set({ 'preferred-package-manager': packageManager })
}

export const setStoredSecondInstallBoxWithDevArgument = (enable: boolean): void => {
  chrome.storage.local.set({ 'second-install-box-with-dev-argument': enable })
}

export const setStoredShouldEnforceLineBreaks = (enable: boolean): void => {
  chrome.storage.local.set({ 'should-enforce-line-breaks': enable })
}

export const setStoredYarnpkgAutoExpandReadme = (enable: boolean): void => {
  chrome.storage.local.set({ 'yarnpkg-auto-expand-readme': enable })
}

export const setStoredToggleContextMenuSearchNpmjs = (toggle: boolean): void => {
  chrome.storage.local.set({ 'toggle-context-menu-search-npmjs': toggle })
}

export const setStoredToggleContextMenuSearchYarnpkg = (toggle: boolean): void => {
  chrome.storage.local.set({ 'toggle-context-menu-search-yarnpkg': toggle })
}
