import '@css/npmjs-install-copy-box.css'

import {
  appendInstallCopyBoxCopyEvent,
  getStoredPreferredPackageManager,
  getStoredSecondInstallBoxWithDevArgument,
  getStoredShouldEnforceLineBreaks,
  isPackagePage,
  replaceInstallCommandCopyBoxContent,
  setEnforceLineBreakInDocument,
  traverseMarkdownInstallCommands
} from './modules'
import { traverseMarkdownCodeBlocks } from './modules/traverseMarkdownCodeBlocks'

document.body.classList.add('pjpm-npmjs-website')

let lastKnowUrl: string
let lastKnowCodeInstalElement: HTMLElement

let copiedCodeInstallContainer: HTMLElement
let copiedCodeInstallBox: HTMLElement
let copiedCodeInstallSaveDevBox: HTMLElement

const updateReadmeInstallCodeBlocks = async (): Promise<void> => {
  const packageManager = await getStoredPreferredPackageManager()

  const selector = '#readme .editor, #readme .highlight > pre, #readme pre > code'
  const codeElements: NodeListOf<HTMLElement> = document.querySelectorAll(selector)

  traverseMarkdownCodeBlocks(codeElements)
  traverseMarkdownInstallCommands(codeElements, packageManager)
}

const updateInstallCopyBox = async (): Promise<void> => {
  const codeInstallSelector = 'code[title="Copy Command to Clipboard"]'
  const codeInstall = document.querySelector(codeInstallSelector) as HTMLElement

  const packageManager = await getStoredPreferredPackageManager()
  const useSecondCopyBoxWithDevArgument = await getStoredSecondInstallBoxWithDevArgument()

  const appendCopiedInstallBoxes = (): void => {
    const codeInstallParent = codeInstall.parentElement as HTMLElement

    if (!codeInstallParent.contains(copiedCodeInstallBox)) {
      codeInstall.insertAdjacentElement('afterend', copiedCodeInstallBox)
    }

    if (useSecondCopyBoxWithDevArgument) {
      codeInstallParent.insertAdjacentElement('afterend', copiedCodeInstallContainer)
    } else copiedCodeInstallContainer.remove()
  }

  if (codeInstall !== null && codeInstall !== lastKnowCodeInstalElement) {
    lastKnowCodeInstalElement = codeInstall

    copiedCodeInstallBox = codeInstall.cloneNode(true) as HTMLElement
    copiedCodeInstallBox.classList.add('pjpm-copy-box')
    appendInstallCopyBoxCopyEvent(copiedCodeInstallBox)

    copiedCodeInstallSaveDevBox = codeInstall.cloneNode(true) as HTMLElement
    copiedCodeInstallSaveDevBox.classList.add('pjpm-copy-box')
    appendInstallCopyBoxCopyEvent(copiedCodeInstallSaveDevBox)

    codeInstall.style.display = 'none'
    copiedCodeInstallContainer = codeInstall.parentElement?.cloneNode(true) as HTMLElement
    copiedCodeInstallContainer.appendChild(copiedCodeInstallSaveDevBox)

    appendCopiedInstallBoxes()
  }

  if (codeInstall !== null) {
    const codeInstallSpan = codeInstall.children[0]

    // Handle npmjs DOM Element removal + changements in user preferences
    appendCopiedInstallBoxes()

    const copiedCodeInstallBoxSpan = copiedCodeInstallBox.getElementsByTagName('span')[0]
    replaceInstallCommandCopyBoxContent(codeInstallSpan, copiedCodeInstallBoxSpan, packageManager)

    if (useSecondCopyBoxWithDevArgument) {
      const span = copiedCodeInstallSaveDevBox.getElementsByTagName('span')[0]
      replaceInstallCommandCopyBoxContent(codeInstallSpan, span, packageManager, true)
    }
  }
}

const update = async (): Promise<void> => {
  if (!isPackagePage()) return

  const currentUrl = location.pathname
  await updateInstallCopyBox()

  if (lastKnowUrl !== currentUrl) {
    lastKnowUrl = currentUrl
    await updateReadmeInstallCodeBlocks()
  }
}

void update().then(() => {
  const observer = new MutationObserver(() => void update())
  const observerOptions: MutationObserverInit = {
    childList     : true,
    characterData : true
  }

  observer.observe(document.getElementsByTagName('title')[0], observerOptions)
})

/* Force Line break in code blocks */

const shouldBreakLines = await getStoredShouldEnforceLineBreaks()
const readmeContainer = document.getElementById('tabpanel-readme')
if (readmeContainer !== null) setEnforceLineBreakInDocument(shouldBreakLines, readmeContainer)
