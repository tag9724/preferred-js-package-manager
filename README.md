# Preferred JS package manager - Web extension

A web extension replacing all install commands with the one of your preferred package manager ( npm, pnpm, yarn ) on `npmjs.com`, `classic.yarnpkg.com` and `yarnpkg.com` package pages.

&nbsp;

**Top right install command copy box will automatically be changed to use your preferred package manager.**

![Npmjs install copy box](screenshots/intall-copy-box-npmjs.png)

&nbsp;

**Readme code blocks with install instructions are modified too with the possibility to display the original content.**

**All code blocks also include a copy button.**

![Npmjs Readme install instruction code block](screenshots/readme-code-block-install-npmjs.gif)

&nbsp;

**Context menus are added to allow a quick search of a selected text on `npmjs.com` and `yarnpkg.com`**

![Context menu quick package search](screenshots/context-menu-search.png)

&nbsp;

**Features can be easely configured on extension options**

![Options](screenshots/configuration.png)

&nbsp;

---

&nbsp;

## Installation

&nbsp;

**With bundle source**

1. Download and extract [preferred-js-package-manager.zip](build/preferred-js-package-manager.zip) located inside the `build` folder.
2. Open the extensions page inside your web browser ( usually `chrome://extensions/` )
3. Enable **developper mode** and click on **load unpacked extension**
4. Select the extracted folder and validate

&nbsp;

**With source code**

If you want to make changes to the source code and bundle it from scratch download or clone the project and install all dependencies :

```
npm install
```

Then you can start the development server with :

```
npm run watch
```

> To add the extension just follow the previous install instructions starting from step 2
