import '@css/preferred-user-style.css'

export const setEnforceLineBreakInDocument = (
  shouldBreakLines: boolean,
  targetElement = document.body
): void => {
  targetElement.classList.toggle('pjpm-enforce-line-breaks', shouldBreakLines)
}
