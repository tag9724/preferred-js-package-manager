import '@css/options-page.css'
import {
  getStoreUserPreferences,
  setStoredPreferredPackageManager,
  setStoredSecondInstallBoxWithDevArgument,
  setStoredShouldEnforceLineBreaks,
  setStoredToggleContextMenuSearchNpmjs,
  setStoredToggleContextMenuSearchYarnpkg,
  setStoredYarnpkgAutoExpandReadme
} from './modules'

const currentPrefs = await getStoreUserPreferences()

/* Preferred package manager */

const preferredPackageManagerInput = document.getElementById(
  'preferred-package-manager'
) as HTMLSelectElement

preferredPackageManagerInput.value = currentPrefs['preferred-package-manager']
preferredPackageManagerInput.onchange = () => {
  setStoredPreferredPackageManager(preferredPackageManagerInput.value as packageManager)
}

/* Toggle forced line breaks in readme codeblocks */

const installBoxWithDevArgument = document.getElementById(
  'install-box-with-dev-argument'
) as HTMLInputElement

installBoxWithDevArgument.checked = currentPrefs['second-install-box-with-dev-argument']
installBoxWithDevArgument.onchange = () => {
  setStoredSecondInstallBoxWithDevArgument(installBoxWithDevArgument.checked)
}

/* Toggle forced line breaks in readme codeblocks */

const forceLineBreaksInput = document.getElementById('enforce-line-breaks') as HTMLInputElement
forceLineBreaksInput.checked = currentPrefs['should-enforce-line-breaks']
forceLineBreaksInput.onchange = () => setStoredShouldEnforceLineBreaks(forceLineBreaksInput.checked)

/* Yarnpkg - Auto expand Readme */

const yarnpkgAutoExpandReadme = document.getElementById(
  'yarnpkg-auto-expand-readme'
) as HTMLInputElement

yarnpkgAutoExpandReadme.checked = currentPrefs['yarnpkg-auto-expand-readme']
yarnpkgAutoExpandReadme.onchange = () =>
  setStoredYarnpkgAutoExpandReadme(yarnpkgAutoExpandReadme.checked)

/* Toggle context menu search on npmjs */

const contextMenuSearchNpmjs = document.getElementById(
  'toggle-context-menu-search-npmjs'
) as HTMLInputElement

contextMenuSearchNpmjs.checked = currentPrefs['toggle-context-menu-search-npmjs']
contextMenuSearchNpmjs.onchange = () => {
  const toggle = contextMenuSearchNpmjs.checked
  const message: runtimeRequest = { type: 'contextMenuToggleNpmjs', toggle }

  setStoredToggleContextMenuSearchNpmjs(toggle)
  chrome.runtime.sendMessage(message)
}

/* Toggle context menu search on yarnpkg */

const contextMenuSearchYarnpkg = document.getElementById(
  'toggle-context-menu-search-yarnpkg'
) as HTMLInputElement

contextMenuSearchYarnpkg.checked = currentPrefs['toggle-context-menu-search-yarnpkg']
contextMenuSearchYarnpkg.onchange = () => {
  const toggle = contextMenuSearchYarnpkg.checked
  const message: runtimeRequest = { type: 'contextMenuToggleYarnpkg', toggle }

  setStoredToggleContextMenuSearchYarnpkg(toggle)
  chrome.runtime.sendMessage(message)
}

/* Display Settings */

document.body.classList.add('loaded')
