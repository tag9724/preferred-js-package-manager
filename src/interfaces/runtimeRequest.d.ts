interface runtimeRequest {
  type: 'contextMenuToggleNpmjs' | 'contextMenuToggleYarnpkg'
  toggle?: boolean
}
