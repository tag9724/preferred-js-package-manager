import '@css/yarnpkg-install-copy-box.css'

import {
  appendInstallCopyBoxCopyEvent,
  getStoredPreferredPackageManager,
  getStoredSecondInstallBoxWithDevArgument,
  getStoredShouldEnforceLineBreaks,
  getStoredYarnpkgAutoExpandReadme,
  isPackagePage,
  replaceInstallCommandCopyBoxContent,
  setEnforceLineBreakInDocument,
  traverseMarkdownInstallCommands
} from './modules'
import { traverseMarkdownCodeBlocks } from './modules/traverseMarkdownCodeBlocks'

const isClassicWebsite = location.host === 'classic.yarnpkg.com'
const installBoxSelectors = {
  regular : 'main > div > div > aside > article h1 ~ div > section',
  classic : '#pkg-detail div.copyable > code'
}

document.body.classList.add('pjpm-yarn-website')
document.body.classList.toggle('pjpm-yarn-website-classic', isClassicWebsite)

const setupInstallCommandCopyBox = async (): Promise<void> => {
  const installCopyBoxselector = installBoxSelectors[isClassicWebsite ? 'classic' : 'regular']
  const installCopyBoxContainer = document.querySelector(installCopyBoxselector) as HTMLElement
  if (installCopyBoxContainer === null) return

  const customInstallCopyBoxContainer = installCopyBoxContainer.cloneNode() as HTMLElement
  customInstallCopyBoxContainer.classList.add('pjpm-install-copy-box')
  customInstallCopyBoxContainer.innerHTML =
    '<div class="pjpm-wrap"><span>...</span><i class="pjpm far fa-clipboard"></i></div>' +
    '<div class="pjpm-wrap none"><span>...</span><i class="pjpm far fa-clipboard"></i></div>'

  const [
    installCopyBox,
    installSaveDevCopyBox
  ] = customInstallCopyBoxContainer.getElementsByTagName('div')

  appendInstallCopyBoxCopyEvent(installCopyBox)
  appendInstallCopyBoxCopyEvent(installSaveDevCopyBox)

  installCopyBoxContainer.parentElement?.classList.add('pjpm-install-copy-box-container')
  installCopyBoxContainer.style.display = 'none'
  installCopyBoxContainer.insertAdjacentElement('afterend', customInstallCopyBoxContainer)

  const [
    installCopyBoxSpan,
    installSaveDevCopyBoxSpan
  ] = customInstallCopyBoxContainer.getElementsByTagName('span')

  const observer = new MutationObserver(() => void changeCopyBox())

  const changeCopyBox = async (): Promise<void> => {
    const span = installCopyBoxContainer.getElementsByTagName('span')[0]

    if (span !== null) {
      const packageManager = await getStoredPreferredPackageManager()
      const useSecondCopyBoxWithDevArgument = await getStoredSecondInstallBoxWithDevArgument()

      replaceInstallCommandCopyBoxContent(span, installCopyBoxSpan, packageManager, false)

      if (useSecondCopyBoxWithDevArgument) {
        replaceInstallCommandCopyBoxContent(span, installSaveDevCopyBoxSpan, packageManager, true)
      }

      installSaveDevCopyBox.classList.toggle('none', !useSecondCopyBoxWithDevArgument)

      installCopyBoxContainer.insertAdjacentElement('afterend', customInstallCopyBoxContainer)
      if (span.textContent !== 'yarn add …') observer.disconnect()
    }
  }

  observer.observe(installCopyBoxContainer, { childList: true, characterData: true, subtree: true })
  void changeCopyBox()
}

const setupReadmeSectionCodeBlocks = async (): Promise<void> => {
  const section = document.getElementById('readme') as HTMLElement
  if (section === null) return

  const observer = new MutationObserver(() => void changeReadmeCodeBlocks())

  const changeReadmeCodeBlocks = async (): Promise<void> => {
    if (section.textContent === 'readme') return
    const packageManager = await getStoredPreferredPackageManager()
    const shouldAutoExpand = await getStoredYarnpkgAutoExpandReadme()

    const readmeCodeBlocks: NodeListOf<HTMLElement> = document.querySelectorAll(
      '#readme pre > code'
    )

    traverseMarkdownCodeBlocks(readmeCodeBlocks)
    traverseMarkdownInstallCommands(readmeCodeBlocks, packageManager)

    if (shouldAutoExpand) {
      const Selector = '[alt="Display full readme"], .readMore--button > img'
      const expandBtn = document.querySelector(Selector)?.parentElement ?? null
      if (expandBtn !== null && expandBtn.nodeName === 'BUTTON') expandBtn.click()
    }

    observer.disconnect()
  }

  observer.observe(section, { childList: true })
  await changeReadmeCodeBlocks()
}

const setupPackagePage = (): void => {
  if (!isPackagePage()) return

  void setupInstallCommandCopyBox()
  void setupReadmeSectionCodeBlocks()
}

/* Init */

if (isClassicWebsite) {
  setupPackagePage()
} else {
  let currentMain: HTMLElement

  const changeMainObserver = (): void => {
    const main = document.getElementsByTagName('main')[0]

    if (currentMain !== main) {
      currentMain = main
      setupPackagePage()

      const observer = new MutationObserver(setupPackagePage)
      observer.observe(main, { childList: true })
    }
  }

  changeMainObserver()

  const observer = new MutationObserver(changeMainObserver)
  const titleElement = document.querySelector('title') as HTMLTitleElement
  observer.observe(titleElement, { childList: true })
}

const shouldBreakLines = await getStoredShouldEnforceLineBreaks()
setEnforceLineBreakInDocument(shouldBreakLines, document.body)
