import '@css/font-awesome.css'
import { joinPaths } from 'utils'

const fontFaceMoz = '/assets/font-awesome-font-face-moz.css'
const fontFaceChrome = '/assets/font-awesome-font-face-chrome.css'

const baseUrl = chrome.runtime.getURL('')
const isMozContext = baseUrl.indexOf('moz-extension://') === 0
const fontFace = isMozContext ? fontFaceMoz : fontFaceChrome

const link = document.createElement('link')
link.setAttribute('rel', 'stylesheet')
link.setAttribute('href', joinPaths(baseUrl, fontFace))
document.head.appendChild(link)
